package com.facundo.apostoles.fotograpp;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.facundo.apostoles.fotograpp.modelos.Especie;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.facundo.apostoles.fotograpp.controllers.EspeciesController;
import com.facundo.apostoles.fotograpp.modelos.Especie;

public class MainActivity extends AppCompatActivity {

    //Método para tomar foto y crear el archivo
    static final int REQUEST_TAKE_PHOTO = 1;
    //Método para mostrar vista previa en un imageview de la foto tomada
    static final int REQUEST_IMAGE_CAPTURE = 1;
    //Método para crear un nombre único de cada fotografia
    String mCurrentPhotoPath;
    static final int CAMERA_REQUEST = 1888;
    private ImageView img;
    private ImageButton btnFoto;
    private int opc = 0;
    private File storageDir;
    private File storageDir2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        img = (ImageView) findViewById(R.id.imageView);

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1000);
        }



    }

    //Método que muestra un popup con las opciones de guardado
    public void showPopup(final String inputPath, final String inputFile, final String outputPath){
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.popup_options);
        dialog.setCancelable(false);
        dialog.show();

        Button btnArbol, btnArbusto;
        btnArbol = (Button) dialog.findViewById(R.id.btnArbol);
        btnArbusto = (Button) dialog.findViewById(R.id.btnArbusto);

        btnArbol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnArbusto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveFile(inputPath,"/"+inputFile,outputPath);
                dialog.dismiss();
            }
        });

    }

    public void onClick(View view){

        Intent ListaEspecies = new Intent(MainActivity.this, EspeciesActivity.class);
        startActivity(ListaEspecies);

    }

    public void onClickI(View view){
        Intent ListaImagenes = new Intent(MainActivity.this, ImagenesActivity.class);
        startActivity(ListaImagenes);
    }

    //Método para mover un archivo a otro directorio
    private void moveFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();


        }

        catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //String directorio = Environment.getExternalStorageDirectory()+"/Fotograpp";
        //File storageDir = new File(directorio);
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        //Se ingresa el directorio deseado
        storageDir = new File("sdcard/Pictures/Fotograpp/Arboles");
        storageDir2 = new File("sdcard/Pictures/Fotograpp/Arbustos");
        //Se crea la carpeta
        storageDir.mkdirs();
        storageDir2.mkdirs();

        /*if (!storageDir.mkdir()){
            Log.e(LOG_TAG, "Directorio no creado");
        }*/
        String imageFileName = "IMG_" + timeStamp + ".jpg";
        System.out.println(storageDir);
        //File img = File.createTempFile(imageFileName, ".jpg", storageDir);
        //File img = new File(storageDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
        File img = new File(storageDir+"/"+imageFileName);


        mCurrentPhotoPath = img.getAbsolutePath();
        System.out.println(mCurrentPhotoPath);
        return img;
    }

    public void tomarFoto(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            // Create the File where the photo should go

            File photoFile = null;
            try {
                photoFile = createImageFile();
                System.out.println("ACA SE MUESTRA EL PATH "+photoFile.getPath());
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.facundo.android.fileprovider", photoFile);
                //Uri photoURI = Uri.fromFile(photoFile);
                //takePictureIntent.setData(photoURI);
                //this.sendBroadcast(takePictureIntent);
                //takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            File imgFile = new File(mCurrentPhotoPath);
            if(imgFile.exists()){
                //Convierte a mapa de bits
                Bitmap imageBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                //Carga la imagen obtenida en el imageView
                img.setImageBitmap(imageBitmap);
                //Muestra un cuadro de dialogo en el que se elige si es un Arbol o Arbusto
                showPopup(storageDir.getPath(),imgFile.getName(),storageDir2.getPath());
                //moveFile(storageDir.getPath(),"/"+imgFile.getName(),storageDir2.getPath());


            }
            /* Codigo original
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView img = (ImageView) findViewById(R.id.imageView);
            img.setImageBitmap(imageBitmap);*/
        }

    }
}

