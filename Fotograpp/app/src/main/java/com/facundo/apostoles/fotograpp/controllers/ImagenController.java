package com.facundo.apostoles.fotograpp.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import com.facundo.apostoles.fotograpp.AyudanteBaseDeDatos;
import com.facundo.apostoles.fotograpp.modelos.Foto;


public class ImagenController {
    private AyudanteBaseDeDatos ayudanteBaseDeDatos;
    private String NOMBRE_TABLA = "imagenes";

    public ImagenController(Context contexto) {
        ayudanteBaseDeDatos = new AyudanteBaseDeDatos(contexto);
    }


    public int eliminarImagen(Foto imagen) {

        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getWritableDatabase();
        String[] argumentos = {String.valueOf(imagen.getId())};
        return baseDeDatos.delete(NOMBRE_TABLA, "id = ?", argumentos);
    }

    public long nuevaImagen(Foto imagen) {
        // writable porque vamos a insertar
        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getWritableDatabase();
        ContentValues valoresParaInsertar = new ContentValues();
        valoresParaInsertar.put("tipo", imagen.getTipo());
        valoresParaInsertar.put("especie", imagen.getEspecie());
        valoresParaInsertar.put("imagen",imagen.getImagen());
        return baseDeDatos.insert(NOMBRE_TABLA, null, valoresParaInsertar);
    }

    public int guardarCambios(Foto imagenEditada) {
        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getWritableDatabase();
        ContentValues valoresParaActualizar = new ContentValues();
        valoresParaActualizar.put("tipo", imagenEditada.getTipo());
        valoresParaActualizar.put("especie", imagenEditada.getEspecie());
        valoresParaActualizar.put("imagen",imagenEditada.getImagen());
        // where id...
        String campoParaActualizar = "id = ?";
        // ... = idImagen
        String[] argumentosParaActualizar = {String.valueOf(imagenEditada.getId())};
        return baseDeDatos.update(NOMBRE_TABLA, valoresParaActualizar, campoParaActualizar, argumentosParaActualizar);
    }

    public ArrayList<Foto> obtenerImagens() {
        ArrayList<Foto> imagenes = new ArrayList<>();
        // readable porque no vamos a modificar, solamente leer
        SQLiteDatabase baseDeDatos = ayudanteBaseDeDatos.getReadableDatabase();
        // SELECT tipo, especie, id
        String[] columnasAConsultar = {"tipo", "especie", "imagen", "id"};
        Cursor cursor = baseDeDatos.query(
                NOMBRE_TABLA,//from imagenes
                columnasAConsultar,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor == null) {
            /*
                Salimos aquí porque hubo un error, regresar
                lista vacía
             */
            return imagenes;

        }
        // Si no hay datos, igualmente regresamos la lista vacía
        if (!cursor.moveToFirst()) return imagenes;

        // En caso de que sí haya, iteramos y vamos agregando los
        // datos a la lista de imagenes
        do {
            // El 0 es el número de la columna, como seleccionamos
            // tipo, especie, imagen, id entonces el tipo es 0, especie 1 e id es 2
            String tipoObtenidoDeBD = cursor.getString(0);
            String especieObtenidaDeBD = cursor.getString(1);
            String imagenObtenidaDeBD = cursor.getString(2);
            long idImagen = cursor.getLong(3);
            Foto fotoObtenidaDeBD = new Foto(tipoObtenidoDeBD, especieObtenidaDeBD, imagenObtenidaDeBD, idImagen);
            imagenes.add(fotoObtenidaDeBD);
        } while (cursor.moveToNext());

        // Fin del ciclo. Cerramos cursor y regresamos la lista de imagenes :)
        cursor.close();
        return imagenes;
    }
}