package com.facundo.apostoles.fotograpp.modelos;

public class Foto {

    private String tipo;
    private String especie;
    private String imagen;

    private long id; // El ID de la BD

    public Foto(String tipo, String especie, String imagen) {
        this.tipo = tipo;
        this.especie = especie;
        this.imagen = imagen;
    }

    // Constructor para cuando instanciamos desde la BD
    public Foto(String tipo, String especie, String imagen, long id) {
        this.tipo = tipo;
        this.especie = especie;
        this.imagen = imagen;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String nombre) {
        this.tipo = tipo;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Tipo{" +
                "especie='" + especie + '\'' +
                ", imagen=" + imagen +
                '}';
    }
}

