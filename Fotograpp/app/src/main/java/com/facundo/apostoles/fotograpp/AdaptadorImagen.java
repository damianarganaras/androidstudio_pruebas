package com.facundo.apostoles.fotograpp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.facundo.apostoles.fotograpp.modelos.Foto;

public class AdaptadorImagen extends RecyclerView.Adapter<AdaptadorImagen.MyViewHolder> {

    private List<Foto> listaDeFotos;

    public void setListaDeFotos(List<Foto> listaDeFotos) {
        this.listaDeFotos = listaDeFotos;
    }

    public AdaptadorImagen(List<Foto> fotos) {
        this.listaDeFotos = fotos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View filaFoto = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fila_imagen, viewGroup, false);
        return new MyViewHolder(filaFoto);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        // Obtener la Foto de nuestra lista gracias al índice i
        Foto Foto = listaDeFotos.get(i);

        // Obtener los datos de la lista
        String tipoFoto = Foto.getTipo();
        String especieFoto = Foto.getEspecie();
        String imagenFoto = Foto.getImagen();
        Bitmap imageBitmap = BitmapFactory.decodeFile(imagenFoto);
        // Y poner a los TextView los datos con setText
        myViewHolder.especie.setText(especieFoto);
        myViewHolder.imagen.setImageBitmap(imageBitmap);
    }

    @Override
    public int getItemCount() {
        return listaDeFotos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView especie, altura, tipo;
        ImageView imagen;

        MyViewHolder(View itemView) {
            super(itemView);
            this.especie = itemView.findViewById(R.id.tvEspecie);
            this.altura = itemView.findViewById(R.id.tvAltura);
            this.tipo = itemView.findViewById(R.id.tvTipo);
            this.imagen = itemView.findViewById(R.id.ivFoto);
        }
    }
}
